import frame.SLFrame;
import panels.SLPanel;

public class WorkWithPanelsExample {
    public static void main(String [] args) {
        SLFrame mainFrame = new SLFrame("Main frame");

        SLPanel mainPanel = new SLPanel(400, 500);
        mainPanel.getComponentsManager()
                .drawComponents("panel", 3)
                .withHeightsProportions(1, 3, 1)
                .workWithComponentAsPanel(1)
                    .drawComponents("verticalPanel", 3)
                    .withWidthsProportions(1, 2, 1)
                    .workWithComponentAsPanel(0)
                        .drawComponents("panel", 2)
                        .goToUpWorkspace()
                    .goToUpWorkspace()
                .workWithComponentAsPanel(2)
                    .drawComponents("verticalPanel", 2);

        mainFrame.setPanel(mainPanel);
    }
}
