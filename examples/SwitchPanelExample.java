import frame.SLFrame;
import panels.SLPanel;

import java.awt.*;
import java.util.Scanner;

public class SwitchPanelExample {
    public static void main(String [] args) {
        SLFrame mainFrame = new SLFrame("Main frame");
        SLPanel redPanel = new SLPanel(300, 200, Color.red);
        SLPanel greenPanel = new SLPanel(300, 400, Color.green);
        SLPanel bluePanel = new SLPanel(150, 400, Color.blue);

        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("**********************");
            System.out.println("Set panel:");
            System.out.println("1 - red panel");
            System.out.println("2 - green panel");
            System.out.println("3 - blue panel");
            System.out.print("Other number - exit: ");
            mainFrame.compileLog();
            switch(scanner.nextInt()) {
                case 1: {
                    mainFrame.setPanel(redPanel);
                    System.out.print(mainFrame.getLog());
                    break;
                } case 2: {
                    mainFrame.setPanel(greenPanel);
                    System.out.print(mainFrame.getLog());
                    break;
                } case 3: {
                    mainFrame.setPanel(bluePanel);
                    System.out.print(mainFrame.getLog());
                    break;
                } default: {
                    System.exit(0);
                    break;
                }
            }
        }
    }
}
