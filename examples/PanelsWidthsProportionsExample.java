import frame.SLFrame;
import panels.SLPanel;

public class PanelsWidthsProportionsExample {
    public static void main(String [] args) {
        SLFrame mainFrame = new SLFrame("Main frame");

        SLPanel mainPanel = new SLPanel(500, 400);
        mainPanel.getComponentsManager()
                .drawComponents("verticalPanel", 3)
                .withWidthsProportions(1, 3, 1);

        mainFrame.setPanel(mainPanel);
    }
}
