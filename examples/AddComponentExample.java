import frame.SLFrame;
import panels.SLPanel;

import javax.swing.*;
import java.awt.*;

public class AddComponentExample {
    public static void main (String [] args) {
        SLFrame mainFrame = new SLFrame("Main frame");

        SLPanel mainPanel = new SLPanel(300, 400);

        GridBagConstraints centerConstraints = new GridBagConstraints();
        centerConstraints.fill = GridBagConstraints.CENTER;
        LayoutManager buttonsPanelLayout = new GridBagLayout();

        Dimension buttonsDimension = new Dimension(80, 25);

        JButton okButton = new JButton("Ok");
        okButton.setPreferredSize(buttonsDimension);

        JButton exitButton = new JButton("Exit");
        exitButton.setPreferredSize(buttonsDimension);

        mainPanel.getComponentsManager()
                .drawComponents("horizontalPanel", 3)
                .withHeightsProportions(1, 3, 1)
                .workWithComponentAsPanel(2)
                    .drawComponents("verticalPanel", 2)
                    .workWithComponentAsPanel(0)
                        .addLayout(buttonsPanelLayout)
                        .addComponent(exitButton, centerConstraints)
                        .goToUpWorkspace()
                    .workWithComponentAsPanel(1)
                        .addLayout(buttonsPanelLayout)
                        .addComponent(okButton, centerConstraints);

        mainFrame.setPanel(mainPanel);
    }
}
