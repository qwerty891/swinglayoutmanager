import componentsManager.ComponentsManager;
import frame.SLFrame;
import panels.SLPanel;

import java.awt.*;

public class ComponentsManagerExample {
    public static void main (String [] args) {
        SLFrame mainFrame = new SLFrame("Main frame");

        SLPanel mainPanel = new SLPanel(300, 400, Color.blue);

        mainPanel.getComponentsManager()
                .drawComponents("panel", 20);

        mainFrame.setPanel(mainPanel);
    }
}
