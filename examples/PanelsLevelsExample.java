import frame.SLFrame;
import panels.SLPanel;
import randomColor.RandomColor;

public class PanelsLevelsExample {
    public static void main(String [] args) {
        SLFrame mainFrame = new SLFrame("Main frame");

        SLPanel mainPanel = new SLPanel(400, 500, RandomColor.getRandomColor());
        mainPanel.getComponentsManager()
                .drawComponents("panel",3)
                .workWithComponentAsPanel(0)
                    .drawComponents("panel", 5)
                    .goToUpWorkspace()
                .workWithComponentAsPanel(1)
                    .drawComponents("panel", 2)
                    .goToUpWorkspace()
                .workWithComponentAsPanel(2)
                    .drawComponents("panel", 3)
                    .goToUpWorkspace();


        mainFrame.setPanel(mainPanel);
    }
}
