import frame.SLFrame;

import java.awt.*;

public class SLFrameExample {
    public static void main(String [] args) {
        /**
         * Without parameters frame.
         */
        SLFrame normalFrame = new SLFrame();

        /**
         * Frame with title.
         */
        SLFrame titleFrame = new SLFrame("Title frame");

        /**
         * Frame with title and size.
         */
        SLFrame slFrame = new SLFrame("Sized frame", 300, 100);

        /**
         * Frame with title, location point and size.
         */
        SLFrame inPointFrame = new SLFrame("In point frame", new Point(100, 100), 300, 100);
    }
}
