import frame.SLFrame;
import panels.SLPanel;

public class PanelsHeightsProportionsExample {
    public static void main (String [] args) {
        SLFrame mainFrame = new SLFrame("Main frame");

        SLPanel mainPanel = new SLPanel(300, 500);
        mainPanel.getComponentsManager()
                .drawComponents("panel", 3)
                .withHeightsProportions(1, 2, 3);

        mainFrame.setPanel(mainPanel);
    }
}
