import frame.SLFrame;
import panels.SLPanel;

import java.awt.*;

public class SLPanelExample {
    public static void main(String [] args) {
        SLPanel slPanel = new SLPanel(300, 100, Color.blue);
        SLFrame slFrame = new SLFrame("Frame with one panel", slPanel);
    }
}
