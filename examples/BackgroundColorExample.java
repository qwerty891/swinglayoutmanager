import frame.SLFrame;
import panels.SLPanel;

import java.awt.*;

public class BackgroundColorExample {
    public static void main (String [] args) {
        SLFrame slFrame = new SLFrame("Main frame");

        SLPanel mainPanel = new SLPanel(500, 600);
        mainPanel.getComponentsManager()
                .setBackgroundColor(Color.red)
                .drawComponents("panel", 3)
                .workWithComponentAsPanel(1)
                    .setBackgroundColor(Color.BLUE)
                    .drawComponents("verticalPanel", 3);

        slFrame.setPanel(mainPanel);
    }
}
