package frame;

import panels.SLPanel;

import javax.swing.*;
import java.awt.*;

public class SLFrame extends JFrame {

    private int defaultCloseOperationValue = JFrame.DISPOSE_ON_CLOSE;
    private boolean isCentered = false;
    private StringBuilder log = new StringBuilder("");

    public SLFrame() {
        setDefaultCloseOperation(defaultCloseOperationValue);
        setLayout(new GridBagLayout());
        setResizable(false);
        pack();
        setLocationRelativeTo(null);
        isCentered = true;
        setVisible(true);
    }

    public SLFrame(String title) {
        setDefaultCloseOperation(defaultCloseOperationValue);
        setLayout(new GridBagLayout());
        setTitle(title);
        setResizable(false);
        pack();
        setLocationRelativeTo(null);
        isCentered = true;
        setVisible(true);
    }

    public SLFrame(String title, int width, int height) {
        setDefaultCloseOperation(defaultCloseOperationValue);
        setLayout(new GridBagLayout());
        setPreferredSize(new Dimension(width, height));
        setTitle(title);
        setResizable(false);
        pack();
        setLocationRelativeTo(null);
        isCentered = true;
        setVisible(true);
    }

    public SLFrame(String title, JPanel panel) {
        setDefaultCloseOperation(defaultCloseOperationValue);
        setLayout(new GridBagLayout());
        add(panel);
        setTitle(title);
        setResizable(false);
        pack();
        setLocationRelativeTo(null);
        isCentered = true;
        setVisible(true);
    }

    public SLFrame(String title, Point inPointLocation, int width, int height) {
        setDefaultCloseOperation(defaultCloseOperationValue);
        setLayout(new GridBagLayout());
        setPreferredSize(new Dimension(width, height));
        setTitle(title);
        setResizable(false);
        pack();
        setLocation(inPointLocation);
        isCentered = false;
        setVisible(true);
    }

    public SLFrame(String title, Point inPointLocation) {
        setDefaultCloseOperation(defaultCloseOperationValue);
        setLayout(new GridBagLayout());
        setTitle(title);
        setResizable(false);
        pack();
        setLocation(inPointLocation);
        isCentered = false;
        setVisible(true);
    }

    public SLFrame setPanel(SLPanel panel) {
        getContentPane().removeAll();
        setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
        getContentPane().add(panel);
        setResizable(true);
        panel.refresh();
        setResizable(false);
        panel.refresh();
        refresh();
        pack();
        setLocationRelativeTo(null);
        return this;
    }

    public SLFrame refresh() {
        revalidate();
        repaint();
        return this;
    }

    public SLFrame compileLog() {
        resetLog();
        makeLog();
        return this;
    }

    private SLFrame resetLog() {
        log.setLength(0);
        return this;
    }

    private SLFrame makeLog() {
        log.append("Components number: " + getContentPane().getComponents().length);
        log.append("\n");
        return this;
    }

    public String getLog() {
        return log.toString();
    }

}
