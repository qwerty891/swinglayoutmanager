package componentsManager;

import com.containers.OnePieceContainer;
import panels.SLPanel;
import randomColor.RandomColor;

import javax.swing.*;
import java.awt.*;

public class ComponentsManager {

    private OnePieceContainer<JComponent> componentsContainer = new OnePieceContainer<>();
    private SLPanel mainPanel;
    private SLPanel upWorkspacePanel;
    private SLPanel simplePanel;
    private Color backgroundColor = null;

    public SLPanel drawMainPanel(int width, int height) {
        mainPanel = new SLPanel(width, height, RandomColor.getRandomColor());
        return mainPanel;
    }

    public ComponentsManager drawComponents(String componentsTypeString, int componentsNumber) {
        try {
            int mainPanelWidth = mainPanel.getPreferredSize().width;
            int mainPanelHeight = mainPanel.getPreferredSize().height;

            switch (componentsTypeString) {
                case "horizontalPanel": {
                    drawHorizontalPanel(mainPanelWidth, mainPanelHeight, componentsNumber);
                    break;
                } case "verticalPanel": {
                    drawVerticalPanel(mainPanelWidth, mainPanelHeight, componentsNumber);
                    break;
                } default: {
                    drawHorizontalPanel(mainPanelWidth, mainPanelHeight, componentsNumber);
                }
            }

            mainPanel.revalidate();
            mainPanel.repaint();
        } catch (Exception ex) {}
        return this;
    }

    private void drawHorizontalPanel(int mainPanelWidth, int mainPanelHeight, int componentsNumber) {
        int panelHeight = mainPanelHeight / componentsNumber;
        for (int counter = 0; counter < componentsNumber; counter++) {
            Color localBackgroundColor = RandomColor.getRandomColor();
            if (backgroundColor != null) localBackgroundColor = backgroundColor;
            SLPanel slPanel = new SLPanel(mainPanelWidth, panelHeight, localBackgroundColor);
            componentsContainer.add(slPanel);
            mainPanel.add(slPanel);
        }
    }

    private void drawVerticalPanel(int mainPanelWidth, int mainPanelHeight, int componentsNumber) {
        int panelWidth = mainPanelWidth / componentsNumber;
        for (int counter = 0; counter < componentsNumber; counter++) {
            Color localBackgroundColor = RandomColor.getRandomColor();
            if (backgroundColor != null) localBackgroundColor = backgroundColor;
            SLPanel slPanel = new SLPanel(panelWidth, mainPanelHeight, localBackgroundColor);
            componentsContainer.add(slPanel);
            mainPanel.add(slPanel);
        }
    }

    public ComponentsManager withHeightsProportions(int ... heightsProportions) {
        try {
            int simplePanelsCount = componentsContainer.size();
            int heightProportionsCount = heightsProportions.length;
            int heightProportionsSum = 0;

            for (int counter = 0; counter < heightProportionsCount; counter++) {
                heightProportionsSum += heightsProportions[counter];
            }

            for (int counter = 0; counter < simplePanelsCount; counter++) {
                JComponent simpleComponent = componentsContainer.getKeyById(counter);
                int oldComponentHeight = simpleComponent.getPreferredSize().height;
                int oldComponentWidth = simpleComponent.getPreferredSize().width;
                float componentHeightProportion = (float) heightsProportions[counter] / heightProportionsSum;
                int newComponentHeight = (int) (oldComponentHeight * componentHeightProportion * heightProportionsCount);
                simpleComponent.setPreferredSize(new Dimension(oldComponentWidth, newComponentHeight));
                simpleComponent.revalidate();
                simpleComponent.repaint();
            }
        } catch (Exception ex) {}


        return this;
    }

    public ComponentsManager withWidthsProportions(int ... widthsProportions) {
        try {
            int simplePanelsCount = componentsContainer.size();
            int widthProportionsCount = widthsProportions.length;
            int widthProportionsSum = 0;

            for (int counter = 0; counter < widthProportionsCount; counter++) {
                widthProportionsSum += widthsProportions[counter];
            }

            for (int counter = 0; counter < simplePanelsCount; counter++) {
                JComponent simpleComponent = componentsContainer.getKeyById(counter);
                int oldComponentHeight = simpleComponent.getPreferredSize().height;
                int oldComponentWidth = simpleComponent.getPreferredSize().width;
                float componentWidthProportion = (float) widthsProportions[counter] / widthProportionsSum;
                int newComponentWidth = (int) (oldComponentWidth * componentWidthProportion * widthProportionsCount);
                simpleComponent.setPreferredSize(new Dimension(newComponentWidth, oldComponentHeight));
                simpleComponent.revalidate();
                simpleComponent.repaint();
            }
        } catch (Exception ex) {}
        return this;
    }

    public JComponent getComponent(int componentNumber) {
        return componentsContainer.getKeyById(componentNumber);
    }

    public SLPanel getMainPanel() {
        return mainPanel;
    }

    public void setMainPanel(SLPanel mainPanel) {
        this.mainPanel = mainPanel;
    }

    public SLPanel getUpWorkspacePanel() {
        return upWorkspacePanel;
    }

    public void setUpWorkspacePanel(SLPanel upWorkspacePanel) {
        this.upWorkspacePanel = upWorkspacePanel;
    }

    public ComponentsManager workWithComponentAsPanel(int componentNumber) {
        try {
            simplePanel = (SLPanel) getComponent(componentNumber);
            simplePanel.setUpWorkspacePanel(mainPanel);
            simplePanel.getComponentsManager().setBackgroundColor(backgroundColor);
        } catch (Exception ex) {}
        return simplePanel.getComponentsManager();
    }

    public ComponentsManager goToUpWorkspace() {
        ComponentsManager componentsManager = null;
        try {
            componentsManager = mainPanel.getUpWorkspacePanel().getComponentsManager();
        } catch (Exception ex) { }
        finally { return componentsManager; }
    }

    public ComponentsManager addComponent(JComponent component) {
        mainPanel.add(component);
        return this;
    }

    public ComponentsManager addComponent(JComponent component, Object constraints) {
        mainPanel.add(component, constraints);
        return this;
    }

    public ComponentsManager addLayout(LayoutManager layoutManager) {
        mainPanel.setLayout(layoutManager);
        return this;
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public ComponentsManager setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
        return this;
    }
}
