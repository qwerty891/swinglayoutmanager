package panels;

import com.containers.TwoPieceContainer;
import componentsManager.ComponentsManager;

import javax.swing.*;
import java.awt.*;

public class SLPanel extends JPanel {

    private TwoPieceContainer<JComponent, String> kidsComponentsList = new TwoPieceContainer();
    private String name = "";
    private ComponentsManager componentsManager = new ComponentsManager();

    public SLPanel(int width, int height) {
        draw(width, height);
        componentsManager.setMainPanel(this);
        setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
    }

    public SLPanel(int width, int height, Color color) {
        draw(width, height, color);
        componentsManager.setMainPanel(this);
        setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
    }

    public SLPanel draw(int width, int height) {
        setPreferredSize(new Dimension(width, height));
        return this;
    }

    public SLPanel draw(int width, int height, Color backgroundColor) {
        setPreferredSize(new Dimension(width, height));
        setBackground(backgroundColor);
        return this;
    }

    public SLPanel getThis() {
        return this;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public ComponentsManager getComponentsManager() {
        return componentsManager;
    }

    public SLPanel refresh() {
        revalidate();
        repaint();
        return this;
    }

    public SLPanel getUpWorkspacePanel() {
        return componentsManager.getUpWorkspacePanel();
    }

    public SLPanel setUpWorkspacePanel(SLPanel upWorkspacePanel) {
        componentsManager.setUpWorkspacePanel(upWorkspacePanel);
        return this;
    }
}
